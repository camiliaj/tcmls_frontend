const path = require('path');
const resolve = (dir) => path.resolve(__dirname, dir);
module.exports = {
  pluginOptions: {
    'style-resources-loader': {
      preProcessor: 'scss',
      patterns: []
    }
  },
  css: {
    loaderOptions: {
      scss: {
        data: `@import "@/assets/style/global.scss";`
      },
    }
  },
  lintOnSave: true,
  // chainWebpack: config => {
  //   config.resolve.alias
  //     .set('config', resolve('config'));
  // },
  devServer: {
    overlay: { // 让浏览器 overlay 同时显示警告和错误
      warnings: true,
      errors: true
    },
    host: "localhost",
    port: 8080,
    https: false,
    open: false,
    hotOnly: true, // 热更新
    proxy: { //配置多个代理
      "/admin/api/v1": {
        target: "http://api.hot-dog.vip",
        changeOrigin: true,
        ws: true,
        secure: false,
        pathRewrite: {
          "^/api/v1": "http://api.hot-dog.vip/admin/api/v1"
        }
      },
      "/api/v1": {
        target: "http://api.hot-dog.vip",
        changeOrigin: true,
        ws: true,
        secure: false,
        pathRewrite: {
          "^/api/v1": "http://api.hot-dog.vip/api/v1"
        }
      },
    }
  }
};