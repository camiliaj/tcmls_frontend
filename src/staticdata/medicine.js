

export const effectMedi = [
  {
    value: '清热药',
    label: '清热药',
    children: [
      {
        label: '清虚热药',
        value: '清虚热药',
      },
      {
        label: '清热解毒药',
        value: '清热解毒药',
      },
      {
        label: '清热泻火药',
        value: '清热泻火药',
      },
      {
        label: '清热凉血药',
        value: '清热凉血药',
      },
      {
        label: '清热燥湿药',
        value: '清热燥湿药',
      },
    ],
  },
  {
    value: '止血药',
    label: '止血药',
    children: [
      {
        label: '收敛止血药',
        value: '收敛止血药',
      },
      {
        label: '凉血止血药',
        value: '凉血止血药',
      },
      {
        label: '化瘀止血药',
        value: '化瘀止血药',
      },
      {
        label: '温经止血药',
        value: '温经止血药',
      },
    ],
  },
  {
    value: '解表药',
    label: '解表药',
    children: [
      {
        label: '辛温解表药',
        value: '辛温解表药',
      },
      {
        label: '辛凉解表药',
        value: '辛凉解表药',
      },
    ],
  },
  {
    value: '安神药',
    label: '安神药',
    children: [
      {
        label: '重镇安神药',
        value: '重镇安神药',
      },
      {
        label: '养心安神药',
        value: '养心安神药',
      },
    ],
  },
  {
    value: '收涩药',
    label: '收涩药',
    children: [
      {
        label: '固表止汗药',
        value: '固表止汗药',
      },
      {
        label: '敛肺涩肠药',
        value: '敛肺涩肠药',
      },
      {
        label: '固精缩尿止带药',
        value: '固精缩尿止带药',
      },
    ],
  },
  {
    value: '补虚药',
    label: '补虚药',
    children: [
      {
        label: '补气药',
        value: '补气药',
      },
      {
        label: '补阳药',
        value: '补阳药',
      },
      {
        label: '补血药',
        value: '补血药',
      },
      {
        label: '补阴药',
        value: '补阴药',
      },
    ],
  },
  {
    value: '活血化瘀药',
    label: '活血化瘀药',
    children: [
      {
        label: '活血止痛药',
        value: '活血止痛药',
      },
      {
        label: '活血调经药',
        value: '活血调经药',
      },
      {
        label: '活血疗伤药',
        value: '活血疗伤药',
      },
      {
        label: '破血消癥药',
        value: '破血消癥药',
      },
    ],
  },
  {
    value: '利水渗湿药',
    label: '利水渗湿药',
    children: [
      {
        label: '利水消肿药',
        value: '利水消肿药',
      },
      {
        label: '利尿通淋药',
        value: '利尿通淋药',
      },
      {
        label: '利湿退黄药',
        value: '利湿退黄药',
      },
    ],
  },
  {
    value: '泻下药',
    label: '泻下药',
    children: [
      {
        label: '攻下药',
        value: '攻下药',
      },
      {
        label: '润下药',
        value: '润下药',
      },
      {
        label: '峻下逐水药',
        value: '峻下逐水药',
      },
    ],
  },
  {
    value: '祛风湿药',
    label: '祛风湿药',
    children: [
      {
        label: '祛风湿散寒药',
        value: '祛风湿散寒药',
      },
      {
        label: '祛风湿清热药',
        value: '祛风湿清热药',
      },
      {
        label: '祛风湿强筋骨药',
        value: '祛风湿强筋骨药',
      },
    ],
  },
  {
    value: '平肝息风药',
    label: '平肝息风药',
    children: [
      {
        label: '平抑肝阳药',
        value: '平抑肝阳药',
      },
      {
        label: '息风止痉药',
        value: '息风止痉药',
      },
    ],
  },
  {
    value: '化痰止咳平喘药',
    label: '化痰止咳平喘药',
    children: [
      {
        label: '温化寒痰药',
        value: '温化寒痰药',
      },
      {
        label: '清化热痰药',
        value: '清化热痰药',
      },
      {
        label: '止咳平喘药',
        value: '止咳平喘药',
      },
    ],
  },
  {
    value: '其他',
    label: '其他',
    children: [
      {
        label: '化湿药',
        value: '化湿药',
      },
      {
        label: '温里药',
        value: '温里药',
      },
      {
        label: '理气药',
        value: '理气药',
      },
      {
        label: '驱虫药',
        value: '驱虫药',
      },
      {
        label: '消食药',
        value: '消食药',
      },
      {
        label: '开窍药',
        value: '开窍药',
      },
      {
        label: '涌吐药',
        value: '涌吐药',
      },
      {
        label: '杀虫止痒药',
        value: '杀虫止痒药',
      },
      {
        label: '拔毒生肌药',
        value: '拔毒生肌药',
      },
    ],
  },
]
export const medicineFormList = [
  {
    label: '中药名',
    type: 'text',
    value: 'mediname',
  },
  {
    label: '中药图片',
    type: 'uploadimage',
    value: 'mediimage',
  },
  {
    label: '别名',
    type: 'text',
    value: 'alias',
  },
  {
    label: '性味归经',
    type: 'select',
    value: 'guijing',
    children: [
      '胃经',
      '脾经',
      '大肠经',
      '肾经',
      '膀胱经',
      '肝经',
      '心经',
      '肺经',
      '小肠经',
      '三焦经',
      '胆经',
      '心包经',
    ],
  },
  {
    label: '功效分类',
    type: 'cascader',
    value: 'effect',
    children: effectMedi
  },
  {
    label: '简介',
    type: 'textarea',
    value: 'profile',
  },
];