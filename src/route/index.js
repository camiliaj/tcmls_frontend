import { createWebHistory, createRouter } from 'vue-router';

import Student from '../pages/Student.vue';
import Index from '../pages/Index.vue';
import notFound from '../pages/404.vue';
import Login from '../pages/Login.vue';
import Home from '../pages/Home.vue';
import Class from '../pages/Class.vue';
import Teacher from '../pages/Teacher.vue';
import SchoolQues from '../pages/SchoolQues.vue';
import SpecialExer from '../pages/SpecialExer.vue';
import MedicineBank from '../pages/MedicineBank.vue';
import Website from '../pages/Website.vue';

const history = createWebHistory();

const route = createRouter({
    history,//路由模式
    routes: [
        {
            path: '/',
            name: 'index',
            component: Index,
            redirect: '/home',
            children: [
                {
                    path: '/home',
                    name: 'Home',
                    component: Home
                },
                {
                    path: '/student',
                    name: 'Student',
                    component: Student
                },
                {
                    path: '/class',
                    name: 'Class',
                    component: Class
                },
                {
                    path: '/teacher',
                    name: 'Teacher',
                    component: Teacher
                },
                {
                    path: '/schoolQues',
                    name: 'SchoolQues',
                    component: SchoolQues
                },
                {
                    path: '/specialExer',
                    name: 'SpecialExer',
                    component: SpecialExer
                },
                {
                    path: '/medicineBank',
                    name: 'MedicineBank',
                    component: MedicineBank
                },
                {
                    path: '/website',
                    name: 'Website',
                    component: Website
                }
            ]
        },

        {
            path: '/:catchAll(.*)',//history模式下要注意服务器配置
            name: '404',
            component: notFound
        },
        {
            path: '/login',
            name: 'Login',
            component: Login
        }
    ]
});
export default route;