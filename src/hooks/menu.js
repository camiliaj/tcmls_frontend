import { ref } from 'vue';
import { Avatar, HomeFilled, List, ElemeFilled, UserFilled, WalletFilled, UploadFilled } from '@element-plus/icons-vue';

// watch(isCollapse, () => {
// });
export const menuList = [
    {
      "name": "首页",
      "path": "/home",
      "role": "all",
      "icon": HomeFilled,
      "divider": false
    },
    {
      "name": "学生管理",
      "path": "/student",
      "role": "all",
      "icon": UserFilled,
      "divider": false
    },
    {
      "name": "教师管理",
      "path": "/teacher",
      "role": "all",
      "icon": Avatar,
      "divider": false
    },
    {
      "name": "班级管理",
      "path": "/class",
      "role": "all",
      "icon": WalletFilled,
      "divider": true
    },
    // {
    //   "name": "学校真题",
    //   "path": "/schoolQues",
    //   "role": "all",
    //   "icon": Management,
    //   "divider": false
    // },
    {
      "name": "题库管理",
      "path": "/specialExer",
      "role": "all",
      "icon": List,
      "divider": false
    },
    {
      "name": "中药管理",
      "path": "/medicineBank",
      "role": "all",
      "icon": UploadFilled,
      "divider": true
    },
    {
      "name": "网站管理",
      "path": "/website",
      "role": "all",
      "icon": ElemeFilled,
      "divider": false
    }
  ];
export const menuActive = ref('');
const currUrl = window?.location.pathname;
const active = menuList.find(v => v.path === currUrl);
export const isCollapse = ref(false);
menuActive.value = active?.name || '首页';

