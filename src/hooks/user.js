export const valiToken = () => {
  const token = JSON.parse(localStorage.getItem('adToken'));
  if (!token) return false;
  if (Date.now() > token.time + token.expire) {
    clearLocal();
    return false;
  }
  return true;
};

export const storeUserInfo = (user, expire) => {
  const { token: mytoken, username, role } = user;
  const token = {
    value: mytoken,
    time: Date.now(),
    expire
  };
  const userInfo = {
    value: { username, role },
    time: Date.now(),
    expire
  };
  clearLocal();
  localStorage.setItem('adToken', JSON.stringify(token));
  localStorage.setItem('aduserInfo', JSON.stringify(userInfo));
};

export const clearLocal = () => {
  localStorage.clear();
};

export function getUserName() {
  return JSON.parse(localStorage.getItem('aduserInfo')).value.username
}