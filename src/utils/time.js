const date = new Date()


export function getTime() {
  const year = date.getFullYear()
  const month = date.getMonth()
  const day = date.getDate()
  const hour = date.getHours()
  const min = date.getMinutes()
  const sec = date.getSeconds()
  return `${year}-${month}-${day} ${hour}:${min}:${sec}`
}