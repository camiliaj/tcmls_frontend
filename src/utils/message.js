import { ElNotification,ElMessage } from 'element-plus';
import { h } from 'vue';
import UploadState from '../fragment/UploadState.vue';

export function sysNoti(msg) {
  ElNotification({
    title: '系统通知',
    dangerouslyUseHTMLString: true,
    icon: h('div',{class:'sys-notice'}),
    message: `<div style="font-size:14px">${msg}</div>`,
  });
}

export function uploading(title) {
  ElNotification({
    title: title,
    dangerouslyUseHTMLString: true,
    icon: h('div',{class:'upload'}),
    duration:6000,
    message:h(UploadState),
  });
}

export function mesErr(msg,duration = 3000) {
  ElMessage({
    message: msg,
    type: 'error',
    duration
  });
}
export function mesSuc(msg,duration = 3000) {
  ElMessage({
    message: msg,
    type: 'success',
    duration
  });
}
export function mesWar(msg) {
  ElMessage({
    message: msg,
    type: 'warning',
  });
}
export function mesInfo(msg) {
  ElMessage(msg);
}
export function sucnotify(title,message="") {
  ElNotification({
    title,
    message,
    type: 'success',
  });
}
export function warnotify(title,message="") {
  ElNotification({
    title,
    message,
    type: 'warning',
  });
}