import mammoth from 'mammoth';

const alpArr = ['A','B','C','D','E','F','G','H','I','J','K']
export function analyzeWord(file) {
  let reader = new FileReader();

  return new Promise((resolve) => {
    reader.onload = function (loadEvent) {
      let arrayBuffer = loadEvent.target.result; //arrayBuffer
      mammoth
        .convertToHtml({ arrayBuffer: arrayBuffer })
        .then((res) => {
          let questionData = [];
          let next = -1;
          res.value.match(/<p>[\s\S]*?<\/p>/ig).forEach((item) => {
            const txt = item.replace(/<p>|<\/p>|<strong>|<\/strong>|<sub>|<\/sub>/g, '');
            // 解析提取出来的文本内容
            const isTopic = /^（/g.test(txt) || /^[0-9]/.test(txt);
            const isAnswer = txt.indexOf('答案') > -1 && !isTopic
            const isAnalysis = txt.indexOf('解析') > -1
            if (isTopic) {
              next += 1;
              let answer = ''
              let topic = ''
              if (/^（/g.test(txt)) {
                const topicInfo = txt.split('答案');
                topic = topicInfo[0].trim().split('）')[1]
                answer = topicInfo[1]?.trim();
              } else {
                topic = txt.split('、')[1]
              }
              questionData.push({ topic, answer, options: [] });
            } else if (isAnswer) {
              questionData[next] && (questionData[next].answer = txt.replace('答案：', '').trim())
            } else if (isAnalysis) {
              questionData[next] && (questionData[next].analysis = txt.replace('解析：', ''))
            } else {
              if (!/([a-zA-Z]、)/g.test(txt)) {
                const beforeOptions = txt.split(/[a-zA-Z]/g);
                const options = [];
                let index = 0;
                beforeOptions?.forEach((vm) => {
                  if(vm) {
                    options.push(alpArr[index]+'、'+vm?.trim());
                    index++;
                  }
                });
                questionData[next]?.options?.push(...options);
              } else {
                const options = txt
                questionData[next]?.options?.push(options);
              }

            }
          });
          resolve(questionData);
        })
        .done();
    };
    reader.readAsArrayBuffer(file);
  });

}

