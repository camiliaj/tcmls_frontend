import { String } from 'core-js';
import { computed } from 'vue'

export const WHMixins = {
    width: {
        type: String || Number,
        require: true
    },
    height: {
        type: String || Number,
        require: true
    },
};

export const getWrapStyle = (props) => {
    return computed(() => {
        //  width & height require 
        if (!props.width) console.warn("'width' property was not passed in");
        if (!props.height) console.warn("'height' property was not passed in");

        const width = typeof props.width === 'number' ? `${props.width}px` : props.width;
        const height = typeof props.height === 'number' ? `${props.height}px` : props.height;
        return {
            width,
            height
        };
    });
};