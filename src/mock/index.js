import Mock from "mockjs";
// import {ref} from 'vue'
export const list = Mock.mock({
  "array|4": [
    {
      classid:'@id',
      classname: "2018级中药学1班",
      headteacher: '@cname',
      teacontact: /^1[385][1-9]\d{8}/,
      createtime: '@date',
      stucount: '100',
      state: 0,
      assessment: Math.floor(Math.random()*5),
      taskcount: 10,
      taskundone: 0,
      examcount: 20,
      examundone: 1,
    },
  ],
});

