import request from "./request";

// 班级列表
export const getClassList = async (params) => {
  return await request.get('/getClassList',params);
};