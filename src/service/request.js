import axios from 'axios';
import qs from 'qs';
import config from './config';
import { mesErr } from '../utils/message';

const service = axios.create(config);

// POST 传参序列化
service.interceptors.request.use(
  config => {
    if (config.method === 'post') {
      config.data = qs.stringify(config.data);
    }
    let tokenObj = JSON.parse(localStorage.getItem('adToken'));
    config.headers.Authorization = tokenObj?.value;
    if (config.baseUrl) {// 扩展baseURL
      config.baseURL = config.baseUrl;
    }
    return config;
  },
  error => {
    return Promise.reject(error);
  }
);

service.interceptors.response.use(
  response => {
    const data = response.data;
    if (response.status === 200 && data.status === 200) {
      // sucnotify(data.message ?? data.msg);
      return data;
    } else {
      // 错误统一处理
      // mesErr(data.message ?? data.msg);
      return Promise.reject();
    }
  },
  error => {
    mesErr('服务器请求失败');
    return Promise.reject(error);
  }
);

export default {
  post(url, data, baseUrl) {
    delete data.createdAt
    delete data.updatedAt
    delete data.deletedAt
    delete data.version
    return service({
      method: 'post',
      url,
      data: data,
      baseUrl
    });
  },
  get(url, data, baseUrl) {
    return service({
      method: 'get',
      url,
      params: data,
      baseUrl
    });
  },
};