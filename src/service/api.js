import request from "./request";
import axios  from "axios";
// 用户登录
export const loginRequest = async (params) => {
  return await request.post('/login', params);
};


// 重要信息
export const getMedicineList = async (params) => {
  return await request.get('/getMedicineList', params);
};

// 班级列表
export const getClassList = async (params) => {
  return await request.get('/getClassList', params);
};

// 更新班级列表
export const updateMedicine = async (params) => {
  return await request.post('/updateMedicine', params);
};
// 新增班级列表
export const addMedicine = async (params) => {
  return await request.post('/addMedicine', params);
}; 

// 删除班级列表
export const deleteMedi = async (params) => {
  return await request.post('/deleteMedi', params);
}; 

// 上传
export const upload = async (file) => {
  let token = JSON.parse(localStorage.getItem('adToken'));
  return axios({
    url: '/api/v1/upload',
    method: 'POST',
    Headers: {
      "content-type": "multipart/form-data",
    },
    data: file,
    params:{token:token.value}
  })
}

//题库
export const addSpeQues = async (params) => {
  return await request.post('/addSpeQues', params);
};   

export const getSpelist = async (params) => {
  return await request.get('/getSpelist', params);
};

export const deleteSpeQ = async (params) => {
  return await request.post('/deleteSpeQ', params);
};

export const updateSpeQ = async (params) => {
  return await request.post('/updateSpeQ', params);
};
export const batchAdd = async (params) => {
  return await request.post('/batchAdd', params);
};

export const getStudentInfo = async (params) => {
  return await request.get('/getStudentInfo', params);
};
// 锁定、解锁账户
export const lockAccount = async (params) => {
  return await request.post('/lock', params);
};

export const getTeacherInfo = async (params) => {
  return await request.get('/getTeacherInfo', params);
};

export const resetPw = async (params) => {
  return await request.post('/resetPw', params);
};

export const getClassInfo = async (params) => {
  return await request.get('/getClassInfo', params);
};
export const startClass = async (params) => {
  return await request.post('/startClass', params);
};
export const closetClass = async (params) => {
  return await request.post('/closetClass', params);
};