export default {
  baseURL: '/admin/api/v1',
  // 自定义的请求头
  headers: {
      'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8"
  },
  timeout: 5000, // 超时设置
  withCredentials: false, // 跨域凭证
  // crossDomain: true,
  // // // 响应的数据格式 json / blob /document /arraybuffer / text / stream
  // responseType: 'json',
  // // XSRF 设置
  // xsrfCookieName: 'XSRF-TOKEN',
  // xsrfHeaderName: 'X-XSRF-TOKEN',
  // // 用于node.js
  // httpAgent: new http.Agent({
  //   keepAlive: true
  // }),
  // httpsAgent: new https.Agent({
  //   keepAlive: true
  // })
};