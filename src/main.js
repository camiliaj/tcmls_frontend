import { createApp } from 'vue';
import router from './route';
import App from './App.vue';
import ElementPlus from 'element-plus';
import 'element-plus/dist/index.css';


import "./assets/style/revise.scss";
import "./assets/style/common.scss";

const app = createApp(App);
window.app = app;
//注入路由
app.use(router);
app.use(ElementPlus);
app.mount('#app');
